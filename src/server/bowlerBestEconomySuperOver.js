function bowlerBestEconomySuperOver(deliveriesData) {
  let obj = {};

  deliveriesData
    .filter((delivery) => {
      return delivery.is_super_over == 1;
    })
    .forEach((delData) => {
      //console.log("in");
      if (obj[delData.bowler] === undefined) {
        obj[delData.bowler] = {};
        obj[delData.bowler]["name"] = delData.bowler;
        if (obj[delData.bowler]["totalRuns"] === undefined) {
          obj[delData.bowler]["totalRuns"] = parseInt(
            delData.total_runs - delData.legbye_runs - delData.penalty_runs
          );
        }
      } else {
        obj[delData.bowler]["totalRuns"] += parseInt(
          delData.total_runs - delData.legbye_runs - delData.penalty_runs
        );
      }
      if (obj[delData.bowler]["ballCount"] === undefined) {
        if (
          parseInt(delData.wide_runs) === 0 &&
          parseInt(delData.noball_runs) === 0
        ) {
          obj[delData.bowler]["ballCount"] = 1;
        }
      } else {
        if (
          parseInt(delData.wide_runs) === 0 &&
          parseInt(delData.noball_runs) === 0
        ) {
          obj[delData.bowler]["ballCount"] += 1;
        }
      }
      obj[delData.bowler]["superOver"] = parseFloat(
        (obj[delData.bowler].totalRuns / obj[delData.bowler].ballCount) * 6
      ).toFixed(2);
    });

  let count = [];

  //console.log(obj);
  for (let prop in obj) {
    count.push(obj[prop]);
  }
  //console.log(count);
  let sorting = count.sort((a, b) => a.superOver - b.superOver);
  return sorting.slice(0, 1);
  //console.log(sorting);
}
module.exports = bowlerBestEconomySuperOver;
