function matchWinnerCount(data) {
  for (let prop in data) {
    if (data[prop].winner === "") {
      data[prop].winner = "match tie";
    }
  }
  let year = data.map((match) => match.season);
  let uniqueYear = [...new Set(year)];

  let winnerPerYear = uniqueYear.map((yearCount) => {
    let yearMatchs = data.filter((match) => yearCount === match.season);
    let winner = yearMatchs.map((match) => match.winner);
    let uniqueWinner = [...new Set(winner)];
    let matchWin = uniqueWinner.map((win) => [
      win,
      winner.filter((won) => won === win).length,
    ]);
    return [yearCount, Object.fromEntries(matchWin)];
  });
  return Object.fromEntries(winnerPerYear);
}

module.exports = matchWinnerCount;
