function strikeRateOfBatsman(deliveriesData, matchData) {
  let obj = {};
  for (let data of deliveriesData) {
    for (let match of matchData) {
      let year = match.season;
      let batsman = data.batsman;
      let batsmanRuns = data.batsman_runs;
      if (data.match_id === match.id) {
        if (obj[batsman] === undefined) {
          obj[batsman] = {};
          obj[batsman][year] = {};
          obj[batsman][year]["totalRuns"] = parseInt(batsmanRuns);
          obj[batsman][year]["ballCount"] = 0;
          if (parseInt(data.wide_runs) === 0) {
            obj[batsman][year]["ballCount"] = 1;
          }
        } else {
          if (obj[batsman][year] === undefined) {
            obj[batsman][year] = {};
            obj[batsman][year]["totalRuns"] = parseInt(batsmanRuns);
            obj[batsman][year]["ballCount"] = 0;
            if (parseInt(data.wide_runs) === 0) {
              obj[batsman][year]["ballCount"] = 1;
            }
          } else {
            obj[batsman][year]["totalRuns"] += parseInt(batsmanRuns);
            if (parseInt(data.wide_runs) === 0) {
              obj[batsman][year]["ballCount"] += 1;
            }
          }
        }
        obj[batsman][year]["strikeRate"] = parseFloat(
          (obj[batsman][year].totalRuns / obj[batsman][year].ballCount) * 100
        ).toFixed(2);
      }
    }
  }
  return obj;
}
module.exports = strikeRateOfBatsman;
