function topEconomicalBowlers(deliveriesData, matchData) {
  let obj = {};

  for (let macData of matchData) {
    for (let delData of deliveriesData) {
      if (delData.match_id === macData.id && macData.season === "2015") {
        if (obj[delData.bowler] === undefined) {
          obj[delData.bowler] = {};
          obj[delData.bowler]["name"] = delData.bowler;
          if (obj[delData.bowler]["totalRuns"] === undefined) {
            obj[delData.bowler]["totalRuns"] = parseInt(
              delData.total_runs - delData.legbye_runs - delData.penalty_runs
            );
          }
        } else {
          obj[delData.bowler]["totalRuns"] += parseInt(
            delData.total_runs - delData.legbye_runs - delData.penalty_runs
          );
        }
        if (obj[delData.bowler]["ballCount"] === undefined) {
          if (
            parseInt(delData.wide_runs) === 0 &&
            parseInt(delData.noball_runs) === 0
          ) {
            obj[delData.bowler]["ballCount"] = 1;
          }
        } else {
          if (
            parseInt(delData.wide_runs) === 0 &&
            parseInt(delData.noball_runs) === 0
          ) {
            obj[delData.bowler]["ballCount"] += 1;
          }
        }
        obj[delData.bowler]["economicalBowlers"] = parseFloat(
          (obj[delData.bowler].totalRuns / obj[delData.bowler].ballCount) * 6
        ).toFixed(2);
      }
    }
  }
  let count = [];

  //console.log(obj);
  for (let prop in obj) {
    count.push(obj[prop]);
  }
  //console.log(count);
  let sorting = count.sort((a, b) => a.economicalBowlers - b.economicalBowlers);
  return sorting.slice(0, 10);
  //console.log(sorting);
}

module.exports = topEconomicalBowlers;
